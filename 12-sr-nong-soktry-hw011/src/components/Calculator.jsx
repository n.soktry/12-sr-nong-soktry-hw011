import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  Form,
  ListGroup,
} from "react-bootstrap";
import imageCalculator from "../img/calc.png";
import Result from "./Result";

export default class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numbers: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    let fstNumber = Number(this.numOne.value);
    let sndNumber = Number(this.numTwo.value);
    let operator = this.operator.value;
    let result;

    if (this.numOne.value === "" || this.numTwo.value === "") {
      alert("Please input number!");
    } else if (isNaN(this.numOne.value) || isNaN(this.numTwo.value)) {
      alert("Please input valid number!");
    } else {
      switch (operator) {
        case "add":
          result = fstNumber + sndNumber;
          break;
        case "sub":
          result = fstNumber - sndNumber;
          break;
        case "mul":
          result = fstNumber * sndNumber;
          break;
        case "div":
          result = fstNumber / sndNumber;
          break;
        case "mod":
          result = fstNumber % sndNumber;
          break;
        default:
          alert("Please select an operator!");
          break;
      }

      this.setState({
        numbers: this.state.numbers.concat(result),
      });
    }
  }

  renderList() {
    if (this.state.numbers.length === 0) {
      return (
        <Card className="text-danger pl-4 py-2">
          There is nothing in history!
        </Card>
      );
    }
    return this.state.numbers.map((rst, index) => (
      <ListGroup.Item key={index}>{rst} </ListGroup.Item>
    ));
  }

  render() {
    return (
      <Container className="mt-3">
        <Row>
          <Col md={4}>
            <form onSubmit={this.handleSubmit}>
              <Card>
                <Card.Img variant="top" src={imageCalculator} />
                <Card.Body>
                  <Form.Group>
                    <Form.Control
                      type="text"
                      placeholder="Enter a number!"
                      ref={(fstNumber) => (this.numOne = fstNumber)}
                      name="numOne"
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Control
                      type="text"
                      placeholder="Enter a number!"
                      ref={(sndNumber) => (this.numTwo = sndNumber)}
                      name="numTwo"
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Control
                      as="select"
                      ref={(selectOperator) => (this.operator = selectOperator)}
                      name="operator"
                    >
                      <option value="add">+ Addition</option>
                      <option value="sub">- Subtraction</option>
                      <option value="mul">* Multiplication</option>
                      <option value="div">/ Division</option>
                      <option value="mod">% Modulus</option>
                    </Form.Control>
                  </Form.Group>
                  <Button variant="primary" type="submit">
                    Calculate
                  </Button>
                </Card.Body>
              </Card>
            </form>
          </Col>
          <Col md={8}>
            <Result item={this.renderList()} />
          </Col>
        </Row>
      </Container>
    );
  }
}
